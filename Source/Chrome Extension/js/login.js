document.addEventListener('DOMContentLoaded', function(){
	var user = null;
	chrome.cookies.get({url: "http://www.cknox.capstone.passkeeper.com", name: "user"}, function(cookie){
		user = cookie.value;
		sessionStorage.setItem("currentUser", user);
		chrome.storage.sync.set({'passkeeper-user': user}, function(){});
		window.location.replace('cred-display.html');
	});
	if(user == null)
	{
		var login = document.getElementById('login');
		login.addEventListener('click', function(){
			loginUser();
		});
	}
});

function loginUser() {
	var username = document.getElementById('username-login').value;
	var password = document.getElementById('password-login').value;
	
	Parse.User.logIn(username, password, {
		success: function(user) {
			//window.location.replace('cred-display.html');
			chrome.cookies.set({url: "http://www.cknox.capstone.passkeeper.com", name: "user", value: username, expirationDate: (new Date().getTime()/1000)+(3600*24*7)});
			sessionStorage.setItem("currentUser", username);
			var localUser = {'username': username, 'password': password};
			localStorage.setItem('user', JSON.stringify(localUser));
			window.location = 'cred-display.html';
		},
		error: function(user, error) {
			if(error.code==100)
			{
				var localUser = JSON.parse(localStorage.getItem('user'));
				if(username==localUser.username)
				{
					if(password==localUser.password)
					{
						chrome.cookies.set({url: "http://www.cknox.capstone.passkeeper.com", name: "user", value: username, expirationDate: (new Date().getTime()/1000)+(3600*24*7)});
						sessionStorage.setItem("currentUser", username);
						window.location = 'cred-display.html';
					}
				}
			}
			else
			{
				location.reload();
				alert("Login unsuccessful");
			}
		}
	});
}