function load_js(){
	var head= document.getElementsByTagName('body')[0];
	var script1= document.createElement('script');
	script1.type= 'text/javascript';
	script1.src= chrome.extension.getURL('/js/parse-1.2.19.js');
	//head.appendChild(script1);
	
	var script2= document.createElement('script');
	script2.type= 'text/javascript';
	script2.src= chrome.extension.getURL('/js/googleapis-jquery-1.7.2.0.js');
	//head.appendChild(script2);
	
	var script3= document.createElement('script');
	script3.type= 'text/javascript';
	script3.src= chrome.extension.getURL('/js/parse-init.js');
	//head.appendChild(script3);
	
	var aesScript= document.createElement('script');
	aesScript.type= 'text/javascript';
	aesScript.src= chrome.extension.getURL('/js/crypto-js/aes.js');
	
	var padScript= document.createElement('script');
	padScript.type= 'text/javascript';
	padScript.src= chrome.extension.getURL('/js/crypto-js/pad-nopadding-min.js');
	
	var script4= document.createElement('script');
	script4.type= 'text/javascript';
	script4.src= chrome.extension.getURL('/js/cred-partial.js');
	
	head.appendChild(script1);
	head.appendChild(script2);
	head.appendChild(aesScript);
	head.appendChild(padScript);
	
	var millisecondsToWait = 500;
	setTimeout(function() {
		head.appendChild(script3);
	}, millisecondsToWait);
	setTimeout(function() {
		head.appendChild(script4);
	}, millisecondsToWait);
}
load_js();

document.addEventListener('sendUserData', function(e){
	var data = e.detail;
	
	var Credential = Parse.Object.extend("Credentials");
	var credential = new Credential();
	
	chrome.storage.sync.get('passkeeper-user', function(items){
		name = items['passkeeper-user'];
		credential.set("passkeeper_id", name);
		credential.set("domain", data.domain);
		credential.set("username", data.username);
		credential.set("password", data.password);
			
		credential.save(null, {
			success: function(credential) {
				alert('Credential saved');
			},
			error: function(credential, error) {
				alert('Error: ' + error.code + ' ' + error.message);
			}
		});
	});
	//chrome.runtime.sendMessage(data, function(response){});
	//alert(data.domain+data.username+data.password);
});