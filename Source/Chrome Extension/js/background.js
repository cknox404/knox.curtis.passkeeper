chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse){
		var Credential = Parse.Object.extend("Credentials");
		var credential = new Credential();
		
		chrome.cookies.get({url: "http://www.cknox.capstone.passkeeper.com", name: "user"}, function(cookie){
			name = cookie.value;
			credential.set("passkeeper_id", name);
			credential.set("domain", request.domain);
			credential.set("username", request.username);
			credential.set("password", request.password);
				
			credential.save(null, {
				success: function(credential) {
					alert('Credential saved');
				},
				error: function(credential, error) {
					alert('Error: ' + error.message);
				}
			});
		});
	}
)

function padString(str){
	var padding = ' ';
	var size = 16;
	var leftOver = str.length % size;
	var paddingLength = size - leftOver;
	for(var i = 0; i < paddingLength; i++)
	{
		str += padding;
	}
	return str;
}