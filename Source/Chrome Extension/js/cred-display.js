var table;
var passphrase = CryptoJS.enc.Utf8.parse("CKnox_PassKeeper_EncryptionKey_Desktop");
document.addEventListener('DOMContentLoaded', function(){
	table = document.getElementById("cred-table");
	
	var head = table.createTHead();
	var headRow = head.insertRow(0);
	var headDomainCell = headRow.insertCell(0);
	var headUsernameCell = headRow.insertCell(1);
	var headPasswordCell = headRow.insertCell(2);
	var headEditCell = headRow.insertCell(3);
	
	headDomainCell.innerHTML = "<b>Website</b>";
	headUsernameCell.innerHTML = "<b>Username</b>";
	headPasswordCell.innerHTML = "<b>Password</b>";
	
	var Credentials = Parse.Object.extend("Credentials");
	var query = new Parse.Query(Credentials);
	
	var name = sessionStorage.getItem("currentUser");
	query.equalTo("passkeeper_id", name);
	query.find({
	  success: function(results) {
		var objects = JSON.parse(JSON.stringify(results));
		for(var i = 0; i < results.length; i++)
		{
			var r = results[i];
			var data = r.get("domain");
			//var rd = atob(data);
			//var iv = rd.substring(0,16);
			//var ct = rd.substring(16);
			
			//var d = CryptoJS.AES.decrypt(
				//{ciphertext: CryptoJS.enc.Utf8.parse(ct)},
				//CryptoJS.enc.Hex.parse(passphrase)
			//);
			//alert(CryptoJS.enc.Utf8.stringify(d));
			
			var object = objects[i];
			
			var row = table.insertRow(i+1);
			displayResult(object, row);
			var editCell = row.insertCell(3);
			var editButton = document.createElement("BUTTON");
			var editText = document.createTextNode("Edit");
			editButton.appendChild(editText);
			editCell.appendChild(editButton);
			
			editButton.addEventListener('click', function(){
				//window.location = 'edit-cred.html?domain='+object.get("domain")+'+username='object.get("username")+'+password='object.get("password");
				var index = this.parentNode.parentNode.rowIndex;
				//var table = document.getElementById("cred-table");
				var currentRow = table.rows[index];
				var credDomain = currentRow.cells[0].innerHTML;
				var credUsername = currentRow.cells[1].innerHTML;
				var credPassword = currentRow.cells[2].innerHTML;

				sessionStorage.setItem("domain", credDomain);
				sessionStorage.setItem("username", credUsername);
				sessionStorage.setItem("password", credPassword);
				window.location = 'cred-edit.html';
			});
			localStorage.setItem('results', JSON.stringify(results));
		}
	  },
	  error: function(error) {
		if(error.code==100)
		{
			var mainDiv = document.getElementById("main");
			mainDiv.removeChild(document.getElementById("cred-create-form"));
			var results = JSON.parse(localStorage.getItem('results'));
			for(var i = 0; i < results.length; i++)
			{
				var object = results[i];
				var row = table.insertRow(i+1);
				displayResult(object, row);
			}
		}
		else
		{
			alert("Error: " + error.code + " " + error.message);
		}
	  }
	});
});

function displayResult(object, row){

	var key = CryptoJS.enc.Hex.parse('000102030405060708090a0b0c0d0e0f');
	var iv = CryptoJS.enc.Hex.parse('0f0e0d0c0b0a09080706050403020100');
	
	var domainCell = row.insertCell(0);
	var usernameCell = row.insertCell(1);
	var passwordCell = row.insertCell(2);
	
	var domain = CryptoJS.AES.decrypt(object.domain, key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	var username = CryptoJS.AES.decrypt(object.username, key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	var password = CryptoJS.AES.decrypt(object.password, key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	
	domainCell.innerHTML = domain.toString(CryptoJS.enc.Utf8).trim();
	usernameCell.innerHTML = username.toString(CryptoJS.enc.Utf8).trim();
	passwordCell.innerHTML = password.toString(CryptoJS.enc.Utf8).trim();

	//deleteButton.addEventListener('click', function(){
	//var index = this.parentNode.parentNode.rowIndex;
	//var currentRow = table.rows[index];
	//var credDomain = currentRow.cells[0].innerHTML;
	//var credUsername = currentRow.cells[1].innerHTML;
	//var credPassword = currentRow.cells[2].innerHTML;
	//deleteCredential(credDomain, credUsername, credPassword);
	//});
}
