function deleteCred(domain, username, password){
	var Credential = Parse.Object.extend("Credentials");
	var query = new Parse.Query(Credential);
	
	query.equalTo("passkeeper_id", sessionStorage.getItem("currentUser"));
	query.equalTo("domain", domain);
	query.equalTo("username", username);
    query.equalTo("password", password);
	query.find({
		success: function(results) {
			var object = results[0];
			object.destroy({
				success: function(object) {
					
				},
				error: function(object, error){
					alert('Error: ' + error.message);
				}
			});
		},
		error: function(error) {
			alert('Error: ' + error.code + ' ' + error.message);
		}
	});
}