var passphrase = CryptoJS.enc.Base64.parse("Q0tub3hfUGFzc0tlZXBlcl9FbmNyeXB0aW9uS2V5X0Rlc2t0b3A=");
document.addEventListener('DOMContentLoaded', function(){
	var create = document.getElementById('create');
	create.addEventListener('click', function(){
		createCred();
	});
});

function createCred() {
	
	var key = CryptoJS.enc.Hex.parse('000102030405060708090a0b0c0d0e0f');
	var iv = CryptoJS.enc.Hex.parse('0f0e0d0c0b0a09080706050403020100');
	
	var domain = document.getElementById('domain-create').value;
	var username = document.getElementById('username-create').value;
	var password = document.getElementById('password').value;
	
	var encDomain = CryptoJS.AES.encrypt(padString(domain), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	var encUsername = CryptoJS.AES.encrypt(padString(username), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	var encPassword = CryptoJS.AES.encrypt(padString(password), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	
	if(domain===""||username===""||password==="")
	{
		alert('One or more fields left blank');
		location.reload();
	}
	else
	{
		var Credential = Parse.Object.extend("Credentials");
		var credential = new Credential();
		
		var name = sessionStorage.getItem("currentUser");
		credential.set("passkeeper_id", name);
		credential.set("domain", encDomain.toString());
		credential.set("username", encUsername.toString());
		credential.set("password", encPassword.toString());
		
		credential.save(null, {
			success: function(credential) {
				window.location.replace('cred-display.html');
			},
			error: function(credential, error) {
				alert('Error: ' + error.message);
			}
		});
	}
}

function padString(str){
	var padding = ' ';
	var size = 16;
	var leftOver = str.length % size;
	var paddingLength = size - leftOver;
	for(var i = 0; i < paddingLength; i++)
	{
		str += padding;
	}
	return str;
}