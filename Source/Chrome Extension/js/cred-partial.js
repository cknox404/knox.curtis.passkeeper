var save = document.getElementById('passkeeper-save');
save.addEventListener('click', function(){
	var username = sessionStorage.getItem("newUsername");
	var password = sessionStorage.getItem("newPassword");
	//var user = Parse.User.current();
	//alert(document.domain+" "+username+" ");
	
	//chrome.runtime.sendMessage(
		//"nonjmemejgoibimhfjdolhhkoibibkhk",
		//"",
		//{domain: document.domain, username: username, password: password}
		//, function(response){});
		
	var key = CryptoJS.enc.Hex.parse('000102030405060708090a0b0c0d0e0f');
	var iv = CryptoJS.enc.Hex.parse('0f0e0d0c0b0a09080706050403020100');
	
	var encDomain = CryptoJS.AES.encrypt(padString(document.domain), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	var encUsername = CryptoJS.AES.encrypt(padString(username), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	var encPassword = CryptoJS.AES.encrypt(padString(password), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	
	var data = {domain: encDomain.toString(), username: encUsername.toString(), password: encPassword.toString()};
	var event = document.createEvent("CustomEvent");
	event.initCustomEvent("sendUserData", true, true, data);
	document.dispatchEvent(event);
	
	var millisecondsToWait = 500;
	setTimeout(function() {
		removeData();
	}, millisecondsToWait);
	//window.location = chrome.extension.getURL('/index.html');
	//chrome.cookies.get({url: "http://www.cknox.capstone.passkeeper.com", name: "user"}, function(cookie){
		//name = cookie.value;
		
	//});
});

var noSave = document.getElementById('passkeeper-no-save');
noSave.addEventListener('click', function(){
	removeData();
});

function removeData(){
	sessionStorage.removeItem("hasSubmitted");
	sessionStorage.removeItem("newUsername");
	sessionStorage.removeItem("newPassword");
	location.reload();
}

function padString(str){
	var padding = ' ';
	var size = 16;
	var leftOver = str.length % size;
	var paddingLength = size - leftOver;
	for(var i = 0; i < paddingLength; i++)
	{
		str += padding;
	}
	return str;
}