var inputs = document.getElementsByTagName('input');
var hasPassword = false;
var submitButton;
for(var i = 0; i < inputs.length; i++){
	if(inputs[i].type.toLowerCase() == 'password'){
		hasPassword = true;
		passwordText = inputs[i];
	}
	if(inputs[i].type.toLowerCase() == 'submit'){
		submitButton = inputs[i];
	}
}
if(submitButton == undefined){
	var buttons = document.getElementsByTagName('button');
	for(var i = 0; i < buttons.length; i++){
		if(buttons[i].type.toLowerCase() == 'submit'){
			submitButton = buttons[i];
		}
	}
}
if(hasPassword){
	var username;
	var password;
	submitButton.addEventListener('click', function(){
		inputs = document.getElementsByTagName('input');
		for(var i = 0; i < inputs.length; i++){
			if(inputs[i].type.toLowerCase() == 'password'){
				password = inputs[i].value;
			}
			if(inputs[i].type.toLowerCase() == 'text'){
				username = inputs[i].value;
			}
			if(username == null && inputs[i].type.toLowerCase() == 'email'){
				username = inputs[i].value;
			}
		}
		sessionStorage.setItem("newUsername", username);
		sessionStorage.setItem("newPassword", password);
		sessionStorage.setItem("hasSubmitted", true);
	});
}
if(sessionStorage.getItem("hasSubmitted")){
	var xhr = new XMLHttpRequest();
	xhr.open('GET', chrome.extension.getURL('save-cred-partial.html'), true);
	xhr.onreadystatechange = function(){
		if(this.readyState!==4) return;
		if(this.status!==200) return;
		var newDiv = document.createElement('div');
		newDiv.id = 'save-cred-partial-div';
		newDiv.innerHTML = this.responseText;
		var body = document.getElementsByTagName("body")[0];
		body.insertBefore(newDiv, body.firstChild);
	};
	xhr.send();
}