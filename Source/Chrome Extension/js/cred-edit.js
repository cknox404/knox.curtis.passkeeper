document.addEventListener('DOMContentLoaded', function(){
	var domain = sessionStorage.getItem("domain");
	var username = sessionStorage.getItem("username");
	var password = sessionStorage.getItem("password");
	
	document.getElementById('domain-edit').value = domain;
	document.getElementById('username-edit').value = username;
	document.getElementById('password').value = password;
	
	var editButton = document.getElementById('edit');
	editButton.addEventListener('click', function(){
		editCred(domain, username, password);
	});
	
	var deleteButton = document.getElementById('delete');
	deleteButton.addEventListener('click', function(){
		deleteCred(domain, username, password);
	});
});

function editCred(oldDomain, oldUsername, oldPassword){

	var key = CryptoJS.enc.Hex.parse('000102030405060708090a0b0c0d0e0f');
	var iv = CryptoJS.enc.Hex.parse('0f0e0d0c0b0a09080706050403020100');
	
	var newDomain = document.getElementById('domain-edit').value;
	var newUsername = document.getElementById('username-edit').value;
	var newPassword = document.getElementById('password').value;
	
	var Credential = Parse.Object.extend("Credentials");
	var query = new Parse.Query(Credential);
	
	var encOldDomain = CryptoJS.AES.encrypt(padString(oldDomain), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	var encOldUsername = CryptoJS.AES.encrypt(padString(oldUsername), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	var encOldPassword = CryptoJS.AES.encrypt(padString(oldPassword), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	
	query.equalTo("passkeeper_id", sessionStorage.getItem("currentUser"));
	query.equalTo("domain", encOldDomain.toString());
	query.equalTo("username", encOldUsername.toString());
    query.equalTo("password", encOldPassword.toString());
	query.find({
		success: function(results) {
			var object = results[0];
			object.set("domain", CryptoJS.AES.encrypt(padString(newDomain), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC}).toString());
			object.set("username", CryptoJS.AES.encrypt(padString(newUsername), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC}).toString());
			object.set("password", CryptoJS.AES.encrypt(padString(newPassword), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC}).toString());
			object.save(null, {
				success: function(credential) {
					window.location.replace('cred-display.html');
				},
				error: function(credential, error) {
					alert('Error: ' + error.message);
				}
			});
		},
		error: function(error) {
			alert("Error: " + error.code + " " + error.message);
		}
	});
}

function deleteCred(domain, username, password){
	
	var key = CryptoJS.enc.Hex.parse('000102030405060708090a0b0c0d0e0f');
	var iv = CryptoJS.enc.Hex.parse('0f0e0d0c0b0a09080706050403020100');
	
	var Credential = Parse.Object.extend("Credentials");
	var query = new Parse.Query(Credential);
	
	var encDomain = CryptoJS.AES.encrypt(padString(domain), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	var encUsername = CryptoJS.AES.encrypt(padString(username), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	var encPassword = CryptoJS.AES.encrypt(padString(password), key, {iv: iv, padding: CryptoJS.pad.NoPadding, mode: CryptoJS.mode.CBC});
	
	query.equalTo("passkeeper_id", sessionStorage.getItem("currentUser"));
	query.equalTo("domain", encDomain.toString());
	query.equalTo("username", encUsername.toString());
    query.equalTo("password", encPassword.toString());
	query.find({
		success: function(results) {
			var object = results[0];
			object.destroy({
				success: function(object) {
					window.location.replace('cred-display.html');
				},
				error: function(object, error){
					alert('Error: ' + error.message);
				}
			});
		},
		error: function(error) {
			alert('Error: ' + error.code + ' ' + error.message);
		}
	});
}

function padString(str){
	var padding = ' ';
	var size = 16;
	var leftOver = str.length % size;
	var paddingLength = size - leftOver;
	for(var i = 0; i < paddingLength; i++)
	{
		str += padding;
	}
	return str;
}