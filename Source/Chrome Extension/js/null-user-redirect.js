document.addEventListener('DOMContentLoaded', function(){
	var currentUser = sessionStorage.getItem("currentUser");
	if(currentUser == null)
	{
		alert('You must be logged in');
		window.location.replace('index.html');
	}
});