document.addEventListener('DOMContentLoaded', function(){
	var logout = document.createElement("BUTTON");
	var text = document.createTextNode("Logout");
	logout.appendChild(text);
	document.body.appendChild(logout);
	
	logout.addEventListener('click', function(){
		logoutUser();
	});
});

function logoutUser() {
	sessionStorage.removeItem("currentUser");
	chrome.cookies.remove({url: "http://www.cknox.capstone.passkeeper.com", name: "user"});
	Parse.User.logOut();
	//window.location.replace('index.html');
	window.location = 'index.html';
}