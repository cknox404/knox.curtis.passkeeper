var OPTIONS = {
	LOWERCASE: {range: 26, index: 97},
	UPPERCASE: {range: 26, index: 65},
	NUMBERS: {range: 10, index: 48}
};

document.addEventListener('DOMContentLoaded', function(){
	var generate = document.getElementById('generate-password');
	generate.addEventListener('click', function(){
		generatePassword();
	});
});

function generatePassword()
{
	var passwordSize = document.getElementById('password-size');
	var size = passwordSize.value;
	if(!(size < passwordSize.min && size > passwordSize.max))
	{
		var isLowercase = document.getElementById('lowercase').checked;
		var isUppercase = document.getElementById('uppercase').checked;
		var isNumeric = document.getElementById('numbers').checked;
		
		var index = 0;
		var options = {};
		if(isLowercase)
		{
			options[index] = OPTIONS.LOWERCASE;
			index++;
		}
		if(isUppercase)
		{
			options[index] = OPTIONS.UPPERCASE;
			index++;
		}
		if(isNumeric)
		{
			options[index] = OPTIONS.NUMBERS;
			index++;
		}
		
		if(index > 0)
		{
			var password = "";
			for(var i = 0; i < size; i++)
			{
				var n = Math.floor(Math.random()*index);
				var option = options[n];
				var chr = String.fromCharCode(Math.floor((Math.random()*option.range)+option.index));
				password+=chr;
			}
			var passwordInput = document.getElementById('password');
			passwordInput.value = password;
		}
	}
}