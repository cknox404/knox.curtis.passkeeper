package com.example.PassKeeper_Android;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public enum Option {
    LOWER_CASE("lowercase", 26, 97), UPPER_CASE("uppercase", 26, 65), NUMBER("numbers", 10, 48);

    private String name;
    private int size;
    private int index;
    private Random random;
    private Option(String name, int size, int index)
    {
        this.name = name;
        this.size = size;
        this.index = index;
        this.random = new Random(System.currentTimeMillis());
    }

    public char getRandomChar()
    {
        return Character.toChars(random.nextInt(size)+index)[0];
    }

    public boolean equalsName(String name)
    {
        return this.name.equalsIgnoreCase(name);
    }
}
