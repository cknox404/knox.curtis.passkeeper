package com.example.PassKeeper_Android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class LoginActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

    }

    public void login(View v)
    {
        final String username = ((EditText)findViewById(R.id.login_username)).getText().toString();
        final String password = ((EditText)findViewById(R.id.login_password)).getText().toString();
        ParseUser.logInInBackground(username, password, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if(e == null)
                {
                    new DAL().saveUserToFile(username, password, getBaseContext());
                    Intent intent = new Intent(getBaseContext(), MainMenuActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("username", parseUser.getUsername());
                    startActivity(intent);
                }
                else if(e.getCode()==ParseException.CONNECTION_FAILED)
                {
                    if(new DAL().loginUser(username, password, getBaseContext()))
                    {
                        Intent intent = new Intent(getBaseContext(), MainMenuActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("username", username);
                        startActivity(intent);
                    }
                }
                else
                {
                    Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });
    }
}