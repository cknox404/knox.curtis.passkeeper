package com.example.PassKeeper_Android;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class Credential implements Serializable {

    private String passKeeperId;
    private String domain;
    private String username;
    private String password;

    public Credential(String passKeeperId, String domain, String username, String password)
    {
        this.passKeeperId = passKeeperId;
        this.domain = domain;
        this.username = username;
        this.password = password;
    }

    @Override
    public String toString()
    {
        return getDomain()+" | "+getUsername()+" | "+getPassword();
    }

    public String getPassKeeperId() {
        return passKeeperId;
    }

    public void setPassKeeperId(String passKeeperId) {
        this.passKeeperId = passKeeperId;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
