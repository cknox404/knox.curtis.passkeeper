package com.example.PassKeeper_Android;

import android.content.Context;
import android.widget.Toast;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class DAL {

    public static final String USER_FILE = "users";
    public static final String CRED_FILE = "credentials";

    public List<Credential> getCredentialsByUsername(String username, Context context)
    {
        List<Credential> credentialList = new ArrayList<Credential>();
        List<ParseObject> parseObjects;
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Credentials");
        query.whereEqualTo("passkeeper_id", username);

        try{
            parseObjects = query.find();
            if(!parseObjects.isEmpty())
            {
                for(ParseObject p : parseObjects)
                {
                    String domain = AES.decrypt(p.getString("domain")).trim();
                    String credUsername = AES.decrypt(p.getString("username")).trim();
                    String password = AES.decrypt(p.getString("password")).trim();
                    credentialList.add(new Credential(username, domain, credUsername, password));
                }
                saveUserCredentialsToFile(username, context);
            }
        }catch (ParseException e){
            if(e.getCode()==ParseException.CONNECTION_FAILED)
            {
                //credentialList = getCredentialsFromFile(context);
                for(Credential cred : getCredentialsFromFile(context))
                {
                    String id = cred.getPassKeeperId();
                    String localDomain = AES.decrypt(cred.getDomain()).trim();
                    String localUsername = AES.decrypt(cred.getDomain()).trim();
                    String localPassword = AES.decrypt(cred.getPassword()).trim();
                    credentialList.add(new Credential(id, localDomain, localUsername, localPassword));
                }
            }
        }
        return credentialList;
    }

    public void createNewUserCredential(Credential credential, Context context)
    {
        String id = credential.getPassKeeperId();
        String domain = AES.encrypt(credential.getDomain());
        String username = AES.encrypt(credential.getUsername());
        String password = AES.encrypt(credential.getPassword());

        ParseObject newCredential = new ParseObject("Credentials");
        newCredential.put("passkeeper_id", id);
        newCredential.put("domain", domain);
        newCredential.put("username", username);
        newCredential.put("password", password);

        String message = "Credential Created";
        try{
            newCredential.save();
            saveUserCredentialsToFile(id, context);
        }catch (ParseException e)
        {
            message = "Error: "+e.getMessage();
        }
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public void editCredential(Credential oldCredential, Credential newCredential, Context context)
    {
        String id = oldCredential.getPassKeeperId();
        String oldDomain = AES.encrypt(oldCredential.getDomain());
        String oldUsername = AES.encrypt(oldCredential.getUsername());
        String oldPassword= AES.encrypt(oldCredential.getPassword());
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Credentials");
        query.whereEqualTo("passkeeper_id", id);
        query.whereEqualTo("domain", oldDomain);
        query.whereEqualTo("username", oldUsername);
        query.whereEqualTo("password", oldPassword);

        String message = "Credential Saved";
        try{
            List<ParseObject> creds = query.find();
            if(!creds.isEmpty())
            {
                String newDomain = AES.encrypt(newCredential.getDomain());
                String newUsername = AES.encrypt(newCredential.getUsername());
                String newPassword = AES.encrypt(newCredential.getPassword());
                ParseObject cred = creds.get(0);
                cred.put("domain", newDomain);
                cred.put("username", newUsername);
                cred.put("password", newPassword);
                cred.save();
                saveUserCredentialsToFile(id, context);
            }
        }catch (ParseException e){
            message = "Error: "+e.getMessage();
        }
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public void deleteCredential(Credential credential, Context context)
    {
        String id = credential.getPassKeeperId();
        String domain = AES.encrypt(credential.getDomain());
        String username = AES.encrypt(credential.getUsername());
        String password = AES.encrypt(credential.getPassword());
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Credentials");
        query.whereEqualTo("passkeeper_id", id);
        query.whereEqualTo("domain", domain);
        query.whereEqualTo("username", username);
        query.whereEqualTo("password", password);

        String message = "Credential Deleted";
        try{
            List<ParseObject> cred = query.find();
            ParseObject object = cred.get(0);
            object.delete();
            saveUserCredentialsToFile(id, context);
        }catch (ParseException e){
            message = "Error: "+e.getMessage();
        }
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public boolean loginUser(String username, String password, Context context)
    {
        List<User> users = getUsersFromFile(context);
        for(User user : users)
        {
            if(user.getUsername().equals(username))
            {
                if (user.getPassword().equals(password))
                {
                    return true;
                }
            }
        }
        return false;
    }

    private List<User> getUsersFromFile(Context context)
    {
        List<User> users;
        try{
            FileInputStream fileIn = context.openFileInput(USER_FILE);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            users = (List<User>)objectIn.readObject();

            objectIn.close();
            fileIn.close();
        }catch (Exception e){
            users = new ArrayList<User>();
        }
        return users;
    }

    public void saveUserToFile(String username, String password, Context context)
    {
        User user = new User(username, password);
        List<User> users = getUsersFromFile(context);
        boolean hasUser = false;
        for(User u : users)
        {
            if(u.getUsername().equals(user.getUsername()))
            {
                hasUser = true;
                break;
            }
        }
        if(!hasUser)
        {
            users.add(user);
            try{
                FileOutputStream fileOut = context.openFileOutput(USER_FILE, Context.MODE_PRIVATE);
                ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
                objectOut.writeObject(users);

                objectOut.close();
                fileOut.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public List<Credential> getCredentialsFromFile(Context context)
    {
        List<Credential> credentialList;
        try{
            FileInputStream fileIn = context.openFileInput(CRED_FILE);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            credentialList = (List<Credential>)objectIn.readObject();

            objectIn.close();
            fileIn.close();
        }catch (Exception e){
            credentialList = new ArrayList<Credential>();
        }
        return credentialList;
    }

    public void saveUserCredentialsToFile(String id, Context context)
    {
        List<Credential> credentialList = new ArrayList<Credential>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Credentials");
        query.whereEqualTo("passkeeper_id", id);
        try{
            List<ParseObject> parseObjects = query.find();

            for(ParseObject p : parseObjects)
            {
                String domain = p.getString("domain");
                String username = p.getString("username");
                String password = p.getString("password");
                credentialList.add(new Credential(id, domain, username, password));
            }

            FileOutputStream fileOut = context.openFileOutput(CRED_FILE, Context.MODE_PRIVATE);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);

            objectOut.writeObject(credentialList);

            objectOut.close();
            fileOut.close();
        }catch (ParseException e){
            if(e.getCode()==ParseException.CONNECTION_FAILED)
            {
                Toast.makeText(context, "No connection", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
