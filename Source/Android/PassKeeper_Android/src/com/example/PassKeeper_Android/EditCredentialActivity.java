package com.example.PassKeeper_Android;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class EditCredentialActivity extends Activity {

    private List<Option> options;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credential_edit);
        options = new ArrayList<Option>(Option.values().length);

        Credential credential = (Credential)getIntent().getSerializableExtra("credential");

        EditText domain = (EditText)findViewById(R.id.edit_domain);
        EditText username = (EditText)findViewById(R.id.edit_username);
        EditText password = (EditText)findViewById(R.id.edit_password);
        domain.setTag(credential);

        domain.setText(credential.getDomain());
        username.setText(credential.getUsername());
        password.setText(credential.getPassword());

        addOptions();
    }

    public void edit(View v)
    {
        EditText domainText = (EditText)findViewById(R.id.edit_domain);
        EditText usernameText = (EditText)findViewById(R.id.edit_username);
        EditText passwordText = (EditText)findViewById(R.id.edit_password);

        String id = ParseUser.getCurrentUser().getUsername();
        String domain = domainText.getText().toString();
        String username = usernameText.getText().toString();
        String password = passwordText.getText().toString();

        new DAL().editCredential((Credential)domainText.getTag(), new Credential(id, domain, username, password), this);

        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("username", id);
        startActivity(intent);
    }

    public void delete(View v)
    {
        EditText domainText = (EditText)findViewById(R.id.edit_domain);
        EditText usernameText = (EditText)findViewById(R.id.edit_username);
        EditText passwordText = (EditText)findViewById(R.id.edit_password);

        String id = ParseUser.getCurrentUser().getUsername();
        String domain = domainText.getText().toString();
        String username = usernameText.getText().toString();
        String password = passwordText.getText().toString();

        new DAL().deleteCredential(new Credential(id, domain, username, password), this);

        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("username", id);
        startActivity(intent);
    }

    public void generatePassword(View v)
    {
        if(options.isEmpty())
        {
            Toast.makeText(this, "No options selected", Toast.LENGTH_SHORT).show();
        }
        else
        {
            NumberPicker passwordSizePicker = (NumberPicker)findViewById(R.id.password_size_edit);
            int size = passwordSizePicker.getValue();
            String password = PasswordGenerator.generatePassword(size, options);
            EditText passwordText = (EditText)findViewById(R.id.edit_password);
            passwordText.setText(password);
        }
    }

    public void onOptionBoxClicked(View v)
    {
        boolean checked = ((CheckBox)v).isChecked();
        String text = ((CheckBox)v).getText().toString();

        for(Option option : Option.values())
        {
            if(option.equalsName(text))
            {
                if(checked)
                {
                    options.add(option);
                }
                else
                {
                    options.remove(option);
                }
            }
        }
    }

    public void copyToClipboard(View v)
    {
        String password = ((EditText)findViewById(R.id.edit_password)).getText().toString();
        ClipboardManager clipboardManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("password", password);
        clipboardManager.setPrimaryClip(clipData);

        Toast.makeText(this, "Password copied", Toast.LENGTH_SHORT).show();
    }

    private void addOptions()
    {
        NumberPicker passwordSizePicker = (NumberPicker)findViewById(R.id.password_size_edit);
        passwordSizePicker.setMinValue(8);
        passwordSizePicker.setMaxValue(64);
    }

    public void logout(View v)
    {
        MyActivity.logout(this);
    }
}