package com.example.PassKeeper_Android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class CreateCredentialActivity extends Activity {

    private List<Option> options;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.credential_create);
        options = new ArrayList<Option>(Option.values().length);

        addOptions();
    }

    public void create(View v)
    {
        EditText domainText = (EditText)findViewById(R.id.create_domain);
        EditText usernameText = (EditText)findViewById(R.id.create_username);
        EditText passwordText = (EditText)findViewById(R.id.create_password);

        String id = ParseUser.getCurrentUser().getUsername();
        String domain = domainText.getText().toString();
        String username = usernameText.getText().toString();
        String password = passwordText.getText().toString();

        new DAL().createNewUserCredential(new Credential(id, domain, username, password), this);

        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("username", id);
        startActivity(intent);
    }

    public void generatePassword(View v)
    {
        if(options.isEmpty())
        {
            Toast.makeText(this, "No options selected", Toast.LENGTH_SHORT).show();
        }
        else
        {
            NumberPicker passwordSizePicker = (NumberPicker)findViewById(R.id.password_size_create);
            int size = passwordSizePicker.getValue();
            String password = PasswordGenerator.generatePassword(size, options);
            EditText passwordText = (EditText)findViewById(R.id.create_password);
            passwordText.setText(password);
        }
    }

    public void onOptionBoxClicked(View v)
    {
        boolean checked = ((CheckBox)v).isChecked();
        String text = ((CheckBox)v).getText().toString();

        for(Option option : Option.values())
        {
            if(option.equalsName(text))
            {
                if(checked)
                {
                    options.add(option);
                }
                else
                {
                    options.remove(option);
                }
            }
        }
    }

    private void addOptions()
    {
        NumberPicker passwordSizePicker = (NumberPicker)findViewById(R.id.password_size_create);
        passwordSizePicker.setMinValue(8);
        passwordSizePicker.setMaxValue(64);
    }

    public void logout(View v)
    {
        MyActivity.logout(this);
    }
}