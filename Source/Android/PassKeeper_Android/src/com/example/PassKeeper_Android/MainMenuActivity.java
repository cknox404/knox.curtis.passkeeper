package com.example.PassKeeper_Android;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.*;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class MainMenuActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        displayCredentials();
    }

    private void displayCredentials()
    {
        String id = getIntent().getStringExtra("username");
        List<Credential> userCredentials = new DAL().getCredentialsByUsername(id, this);

        if(!userCredentials.isEmpty())
        {
//            ListView credentialList = (ListView)findViewById(R.id.credentials);
//            ArrayAdapter<Credential> adapter = new ArrayAdapter<Credential>(this,
//                    android.R.layout.simple_list_item_1, userCredentials);
//
//            credentialList.setAdapter(adapter);
            TableLayout layout = (TableLayout)findViewById(R.id.credential_table);

            TableRow columnRow = new TableRow(this);
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(20, 2, 20, 2);
            columnRow.setLayoutParams(layoutParams);
            columnRow.setPadding(10, 2, 10, 10);

            TextView domainColumn = new TextView(this);
            TextView usernameColumn = new TextView(this);
            TextView passwordColumn = new TextView(this);
            domainColumn.setText("Website");
            usernameColumn.setText("Username");
            passwordColumn.setText("Password");
            domainColumn.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
            usernameColumn.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
            passwordColumn.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);

            domainColumn.setLayoutParams(layoutParams);
            usernameColumn.setLayoutParams(layoutParams);
            passwordColumn.setLayoutParams(layoutParams);

            columnRow.addView(domainColumn);
            columnRow.addView(usernameColumn);
            columnRow.addView(passwordColumn);
            layout.addView(columnRow, layoutParams);
            for(Credential cred : userCredentials)
            {
                TableRow row = new TableRow(this);
                row.setLayoutParams(layoutParams);
                row.setPadding(10, 2, 10, 2);

                TextView domain = new TextView(this);
                TextView username = new TextView(this);
                TextView password = new TextView(this);
                domain.setText(cred.getDomain());
                username.setText(cred.getUsername());
                password.setText(cred.getPassword());
                domain.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
                username.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
                password.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);

                domain.setLayoutParams(layoutParams);
                username.setLayoutParams(layoutParams);
                password.setLayoutParams(layoutParams);

                row.addView(domain);
                row.addView(username);
                row.addView(password);

                row.setOnClickListener(new RowOnClickListener(cred, this));

                layout.addView(row, layoutParams);
            }
        }
    }

    public void createCredential(View v)
    {
        Intent intent = new Intent(this, CreateCredentialActivity.class);
        startActivity(intent);
    }

    public void clearClipboard(View v)
    {
        ClipboardManager clipboardManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("password", "");
        clipboardManager.setPrimaryClip(clipData);
        Toast.makeText(this, "Clipboard cleared", Toast.LENGTH_SHORT).show();
    }

    public void logout(View v)
    {
        MyActivity.logout(this);
    }

    private class RowOnClickListener implements View.OnClickListener{

        Context context;
        Credential credential;

        public RowOnClickListener(Credential credential, Context context)
        {
            this.context = context;
            this.credential = credential;
        }

        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(context, EditCredentialActivity.class);
            intent.putExtra("credential", credential);
            startActivity(intent);
        }
    }
}