package com.example.PassKeeper_Android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.parse.Parse;
import com.parse.ParseUser;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this, "CyknmOFwTvPzHe9umvAic5tvKw6GiXZoMzVuhA9e", "p65etPPniM8sspxrSEuTZm8PYCzwvFQZ7N6GH2Kp");
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public static void logout(Activity sender)
    {
        ParseUser.logOut();
        Intent intent = new Intent(sender.getBaseContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sender.startActivity(intent);
    }
}
