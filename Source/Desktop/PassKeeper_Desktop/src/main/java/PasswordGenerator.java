import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class PasswordGenerator {

    public static String generatePassword(int size, List<Option> options)
    {
        String password = "";
        Random random = new Random(System.currentTimeMillis());
        for(int i = 0; i < size; i++)
        {
            int next = random.nextInt(options.size());
            char newChar = options.get(next).getRandomChar();
            password+=newChar;
        }
        return password;
    }
}
