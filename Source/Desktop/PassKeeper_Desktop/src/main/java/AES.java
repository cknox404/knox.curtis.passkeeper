import org.apache.commons.codec.binary.Base64;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class AES {

    private static byte[] key = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F };
    //{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    private static byte[] iv = new byte[] { 0x0F, 0x0E, 0x0D, 0x0C, 0x0B, 0x0A, 0x09, 0x08, 0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00 };
    //{ 'f', 'e', 'd', 'c', 'b', 'a', '9', '8', '7', '6', '5', '4', '3', '2', '1', '0' };

    private static IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
    private static SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

    public static String encrypt(String data)
    {
        String paddedString = padString(data);
        String encryptedValue = "";
        try{
            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
            byte[] encryptedBytes = cipher.doFinal(paddedString.getBytes());
            encryptedValue = new BASE64Encoder().encode(encryptedBytes);
        }catch (Exception e){
            e.printStackTrace();
        }
        return encryptedValue;
    }

    public static String decrypt(String data)
    {
        String decryptedValue = "";
        try{
            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
            byte[] decodedBase64 = new BASE64Decoder().decodeBuffer(data);
            byte[] decryptedBytes = cipher.doFinal(decodedBase64);
            decryptedValue = new String(decryptedBytes);
        }catch (Exception e){
            e.printStackTrace();
        }
        return decryptedValue;
    }

    private static String padString(String stringToPad)
    {
        char padding = ' ';
        int size = 16;
        int leftOver = stringToPad.length() % size;
        int paddingLength = size - leftOver;

        for(int i = 0; i < paddingLength; i++)
        {
            stringToPad += padding;
        }
        return stringToPad;
    }
//    private static SecretKeySpec secretKeySpec;
//    private static String encryptedString;
//    private static String decryptedString;
//
//    public static void setKey()
//    {
//        String encryptionKey = "CKnox_PassKeeper_EncryptionKey_Desktop";
//        MessageDigest sha;
//        byte[] key;
//        try{
//            sha = MessageDigest.getInstance("SHA-1");
//            key = encryptionKey.getBytes();
//            key = sha.digest(key);
//            key = Arrays.copyOf(key, 16);
//            secretKeySpec = new SecretKeySpec(key, "AES");
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }
//
//    public static String encrypt(String toEncrypt)
//    {
//        try{
//            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
//            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
//            setEncryptedString(Base64.encodeBase64String(cipher.doFinal(toEncrypt.getBytes("UTF-8"))));
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return encryptedString;
//    }
//
//    public static String decrypt(String toDecrypt)
//    {
//        try{
//            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
//            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
//            setDecryptedString(new String(cipher.doFinal(Base64.decodeBase64(toDecrypt))));
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return decryptedString;
//    }
//
//    public static void setEncryptedString(String encryptedString) {
//        AES.encryptedString = encryptedString;
//    }
//
//    public static void setDecryptedString(String decryptedString) {
//        AES.decryptedString = decryptedString;
//    }
}
