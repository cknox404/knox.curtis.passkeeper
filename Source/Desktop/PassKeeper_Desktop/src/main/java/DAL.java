import org.parse4j.ParseException;
import org.parse4j.ParseObject;
import org.parse4j.ParseQuery;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class DAL {

    public List<Credential> getCredentialsByUsername(String username)
    {
        List<Credential> credentialList = new ArrayList<Credential>();
        List<ParseObject> userCredentials;
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Credentials");
        query.whereEqualTo("passkeeper_id", username);

        try{
            userCredentials = query.find();
            if(userCredentials!=null)
            {
                for(ParseObject cred : userCredentials)
                {
                    String credDomain = AES.decrypt(cred.getString("domain")).trim();
                    String credUsername = AES.decrypt(cred.getString("username")).trim();
                    String credPassword = AES.decrypt(cred.getString("password")).trim();
                    credentialList.add(new Credential(PassKeeper.currentUser.getUsername(), credDomain, credUsername, credPassword));
                }
                saveAllCredentialsToFile();
            }
        }catch (ParseException e){
            if(e.getCode()==ParseException.CONNECTION_FAILED)
            {
                //credentialList = getAllCredentialsFromFile();
                //credentialList = getCredentialsFromFileByUsername(PassKeeper.currentUser.getUsername());
                for(Credential cred : getCredentialsFromFileByUsername(PassKeeper.currentUser.getUsername()))
                {
                    String id = cred.getPassKeeperId();
                    String localDomain = AES.decrypt(cred.getDomain()).trim();
                    String localUsername = AES.decrypt(cred.getDomain()).trim();
                    String localPassword = AES.decrypt(cred.getPassword()).trim();
                    credentialList.add(new Credential(id, localDomain, localUsername, localPassword));
                }
            }
        }
//        query.findInBackground(new FindCallback<ParseObject>() {
//            @Override
//            public void done(List<ParseObject> parseObjects, ParseException e) {
//                if(e == null)
//                {
//                    for(ParseObject p : parseObjects)
//                    {
//                        userCredentials.add(p);
//                    }
//                }
//                else
//                {
//                    System.out.println("Error: " + e.getMessage());
//                }
//            }
//        });
        return credentialList;
    }

    public void addNewCredentialToUser(String passKeeperUsername, Credential credential)
    {
        ParseObject newCredential = new ParseObject("Credentials");

        String domain = AES.encrypt(credential.getDomain());
        String username = AES.encrypt(credential.getUsername());
        String password = AES.encrypt(credential.getPassword());
        System.out.println(domain+" "+username+" "+password);
        newCredential.put("passkeeper_id", passKeeperUsername);
        newCredential.put("domain", domain);
        newCredential.put("username", username);
        newCredential.put("password", password);

        try{
            newCredential.save();
        }catch (ParseException e){
            System.out.println("Error: "+e.getMessage());
        }
        saveAllCredentialsToFile();
    }

    public void editCredential(String passKeeperUsername, Credential oldCredential, Credential newCredential)
    {
        String oldDomain = AES.encrypt(oldCredential.getDomain());
        String oldUsername = AES.encrypt(oldCredential.getUsername());
        String oldPassword= AES.encrypt(oldCredential.getPassword());
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Credentials");
        query.whereEqualTo("passkeeper_id", passKeeperUsername);
        query.whereEqualTo("domain", oldDomain);
        query.whereEqualTo("username", oldUsername);
        query.whereEqualTo("password", oldPassword);
        try{
            List<ParseObject> creds = query.find();
            if(!creds.isEmpty())
            {
                String newDomain = AES.encrypt(newCredential.getDomain());
                String newUsername = AES.encrypt(newCredential.getUsername());
                String newPassword = AES.encrypt(newCredential.getPassword());
                ParseObject cred = creds.get(0);
                cred.put("domain", newDomain);
                cred.put("username", newUsername);
                cred.put("password", newPassword);
                cred.save();
                cred.setUpdatedAt(new Date());
            }
        }catch (ParseException e){
            System.out.println("Error: "+e.getMessage());
        }
        saveAllCredentialsToFile();
    }

    public void deleteCredential(String passKeeperUsername, Credential credential)
    {
        String domain = AES.encrypt(credential.getDomain());
        String username = AES.encrypt(credential.getUsername());
        String password = AES.encrypt(credential.getPassword());
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Credentials");
        query.whereEqualTo("passkeeper_id", passKeeperUsername);
        query.whereEqualTo("domain", domain);
        query.whereEqualTo("username", username);
        query.whereEqualTo("password", password);
        try{
            List<ParseObject> creds = query.find();
            if(!creds.isEmpty())
            {
                creds.get(0).delete();
            }
        }catch (ParseException e){
            System.out.println("Error: "+e.getMessage());
        }
        saveAllCredentialsToFile();
    }

    private List<Credential> getCredentialsFromFileByUsername(String name)
    {
        List<Credential> userCredentials = new ArrayList<Credential>();
        for(Credential credential : getAllCredentialsFromFile())
        {
            if(name.equals(credential.getPassKeeperId()))
            {
                userCredentials.add(credential);
            }
        }
        return userCredentials;
    }

    private List<Credential> getAllCredentialsFromFile()
    {
        List<Credential> credentials = new ArrayList<Credential>();
        try{
            InputStream file = new FileInputStream("credentials.txt");
            InputStream buffer = new BufferedInputStream(file);
            ObjectInput input = new ObjectInputStream(buffer);

            credentials = (List<Credential>)input.readObject();

            input.close();
            buffer.close();
            file.close();
        }catch (Exception e){
            System.out.println("Failed to read");
        }
        return credentials;
    }

    private void saveAllCredentialsToFile()
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Credentials");
        List<Credential> credentialList = new ArrayList<Credential>();
        try{
            List<ParseObject> parseObjects = query.find();
            OutputStream file = new FileOutputStream("credentials.txt");
            OutputStream buffer = new BufferedOutputStream(file);
            ObjectOutput output = new ObjectOutputStream(buffer);

            for(ParseObject cred : parseObjects)
            {
                String passKeeperId = cred.getString("passkeeper_id");
                String credDomain = cred.getString("domain");
                String credUsername = cred.getString("username");
                String credPassword = cred.getString("password");
                credentialList.add(new Credential(passKeeperId, credDomain, credUsername, credPassword));
            }

            output.writeObject(credentialList);

            output.close();
            buffer.close();
            file.close();
        }catch (ParseException e){
            if(e.getCode()==ParseException.CONNECTION_FAILED)
            {
                System.out.println("No Parse connection");
            }
        }catch (Exception ex){
            System.out.println("Failed to write");
        }

    }

    public void saveUserToFile(String username, String password)
    {
        User user = new User(username, password);
        List<User> users = getAllUsersFromFile();
        boolean hasUser = false;
        for(User u : users)
        {
            if(u.getUsername().equals(user.getUsername()))
            {
                hasUser = true;
                break;
            }
        }
        if(!hasUser)
        {
            try{
                OutputStream file = new FileOutputStream("users.txt");
                OutputStream buffer = new BufferedOutputStream(file);
                ObjectOutput output = new ObjectOutputStream(buffer);

                users.add(user);
                output.writeObject(users);

                output.close();
                buffer.close();
                file.close();
            }catch (Exception ex){
                System.out.println("Failed to write");
            }
        }
    }

    public List<User> getAllUsersFromFile()
    {
        List<User> users = new ArrayList<User>();
        try{
            InputStream file = new FileInputStream("users.txt");
            InputStream buffer = new BufferedInputStream(file);
            ObjectInput input = new ObjectInputStream(buffer);

            users = (List<User>)input.readObject();

            input.close();
            buffer.close();
            file.close();
        }catch (Exception e){
            System.out.println("Failed to read");
        }
        return users;
    }

    public boolean loginUser(String username, String password)
    {
        for(User user : getAllUsersFromFile())
        {
            if(user.getUsername().equals(username))
            {
                if(user.getPassword().equals(password))
                {
                    return true;
                }
            }
        }
        return false;
    }
}