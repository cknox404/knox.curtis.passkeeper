import org.parse4j.ParseException;
import org.parse4j.ParseUser;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class PassKeeper {

    private DAL dal;
    public static ParseUser currentUser;

    private final int WIDTH = 1000;
    private final int HEIGHT = 550;
    private JFrame loginFrame;
    private JFrame mainFrame;
//    private JPanel loginPanel;
    private JPanel menuPanel;
    private JPanel credCreatePanel;
//    private JPanel logoutPanel;
    private JPanel editPanel;

    public PassKeeper()
    {
        dal = new DAL();
    }

    public void start()
    {
        addLoginDialog();
    }

    private void addLoginDialog()
    {
        createLoginFrame();
        JPanel loginPanel = new JPanel();
//        loginPanel.setSize(400, HEIGHT);

        final JTextField usernameField = new JTextField(16);
        final JPasswordField passwordField = new JPasswordField(16);
        usernameField.setToolTipText("Username");
        passwordField.setToolTipText("Password");

        JButton loginButton = new JButton("Login");
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String username = usernameField.getText();
                String password = new String(passwordField.getPassword());
                try{
                    currentUser = ParseUser.login(username, password);

                    loginFrame.setVisible(false);
                    loginFrame.dispose();
                    dal.saveUserToFile(username, password);
                    createMainFrame();
                    addUserMenu();
                    addLogoutButton();
                }catch (ParseException ex){
                    if(ex.getCode()==ParseException.CONNECTION_FAILED)
                    {
                        if(dal.loginUser(username, password))
                        {
                            currentUser = new ParseUser();
                            currentUser.setUsername(username);
                            createMainFrame();
                            addUserMenu();
                            addLogoutButton();
                        }
                    }
                }
                loginFrame.revalidate();
                mainFrame.revalidate();
            }
        });

        loginPanel.add(usernameField);
        loginPanel.add(passwordField);
        loginPanel.add(loginButton);

        loginFrame.add(loginPanel);
        loginFrame.setVisible(true);
    }

    private void createLoginFrame()
    {
        loginFrame = new JFrame("PassKeeper");
        loginFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        loginFrame.setSize(400, HEIGHT/4);
        loginFrame.setLocationRelativeTo(null);
//        loginFrame.setVisible(true);
    }

    private void createMainFrame()
    {
        mainFrame = new JFrame("PassKeeper");
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setSize(WIDTH, HEIGHT);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    private void addUserMenu()
    {
        menuPanel = new JPanel(new BorderLayout());
        menuPanel.setBorder(new EmptyBorder(0, WIDTH/4, 0, WIDTH/4));
//        menuPanel.setSize(WIDTH/2, HEIGHT);

        showUserCredentials();
        JButton createButton = new JButton("Add New Credential");
        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeUserMenu();
                addCredentialCreateDialog();

                mainFrame.revalidate();
            }
        });
        JButton clearButton = new JButton("Clear Clipboard");
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StringSelection stringSelection = new StringSelection("");
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
            }
        });
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(createButton);
        buttonPanel.add(clearButton);
        menuPanel.add(buttonPanel, BorderLayout.SOUTH);

        mainFrame.add(menuPanel, BorderLayout.CENTER);
    }

    private void showUserCredentials()
    {
        int columnNumber = 5;
        List<Credential> userCredentials = dal.getCredentialsByUsername(currentUser.getUsername());
        int credentialNumber = userCredentials.size();

        JPanel credentialPanel = new JPanel(new GridLayout(credentialNumber+1, columnNumber));
//        String[][] credentialLabels = new String[credentialNumber][columnNumber];

        JLabel[] columnNames = new JLabel[columnNumber];
        columnNames[0] = new JLabel("Website", SwingConstants.CENTER);
        columnNames[1] = new JLabel("Username", SwingConstants.CENTER);
        columnNames[2] = new JLabel("Password", SwingConstants.CENTER);
        columnNames[3] = new JLabel();
        columnNames[4] = new JLabel();

        columnNames[0].setBorder(BorderFactory.createLineBorder(Color.black));
        columnNames[1].setBorder(BorderFactory.createLineBorder(Color.black));
        columnNames[2].setBorder(BorderFactory.createLineBorder(Color.black));
        columnNames[3].setBorder(BorderFactory.createLineBorder(Color.black));
        columnNames[4].setBorder(BorderFactory.createLineBorder(Color.black));

        credentialPanel.add(columnNames[0]);
        credentialPanel.add(columnNames[1]);
        credentialPanel.add(columnNames[2]);
        credentialPanel.add(columnNames[3]);
        credentialPanel.add(columnNames[4]);

        JLabel[][] credentialLabels = new JLabel[credentialNumber][columnNumber];

        for(int i = 0; i < credentialNumber; i++)
        {
            Credential p = userCredentials.get(i);
            String domain = p.getDomain();
            String username = p.getUsername();
            String password = p.getPassword();
            credentialLabels[i][0] = new JLabel(domain, SwingConstants.CENTER);
            credentialLabels[i][1] = new JLabel(username, SwingConstants.CENTER);
            credentialLabels[i][2] = new JLabel(password, SwingConstants.CENTER);
            credentialLabels[i][3] = new JLabel("Edit", SwingConstants.CENTER);
            credentialLabels[i][4] = new JLabel("Delete", SwingConstants.CENTER);

            credentialLabels[i][2].addMouseListener(new PasswordLabelMouseListener(password));
            credentialLabels[i][2].setOpaque(true);

            credentialLabels[i][3].addMouseListener(new EditLabelActionListener(
                    new Credential(currentUser.getUsername(), domain, username, password)
            ));
            credentialLabels[i][3].setOpaque(true);

            credentialLabels[i][4].addMouseListener(new DeleteLabelMouseListener(
                    new Credential(currentUser.getUsername(), domain, username, password)
            ));
            credentialLabels[i][4].setOpaque(true);

            credentialLabels[i][0].setBorder(BorderFactory.createLineBorder(Color.gray));
            credentialLabels[i][1].setBorder(BorderFactory.createLineBorder(Color.gray));
            credentialLabels[i][2].setBorder(BorderFactory.createLineBorder(Color.gray));
            credentialLabels[i][3].setBorder(BorderFactory.createLineBorder(Color.gray));
            credentialLabels[i][4].setBorder(BorderFactory.createLineBorder(Color.gray));

            credentialPanel.add(credentialLabels[i][0]);
            credentialPanel.add(credentialLabels[i][1]);
            credentialPanel.add(credentialLabels[i][2]);
            credentialPanel.add(credentialLabels[i][3]);
            credentialPanel.add(credentialLabels[i][4]);
        }

        menuPanel.add(credentialPanel, BorderLayout.CENTER);

//        String[] columnNames = {"Domain", "Username", "Password"};
//        JTable credentialTable = new JTable(credentialLabels, columnNames);
//        credentialTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

//        menuPanel.add(credentialTable);
    }

    public void addCredentialCreateDialog()
    {
        credCreatePanel = new JPanel();
        credCreatePanel.setLayout(new GridLayout(8, 5));

        JTextField dField = new JTextField(20);
        JTextField uField = new JTextField(20);
        JTextField pField = new JTextField(20);

        dField.setMaximumSize(new Dimension(100, 24));
        uField.setMaximumSize(new Dimension(300, 24));
        pField.setMaximumSize(new Dimension(300, 24));

        final JTextField domainField = dField;
        final JTextField usernameField = uField;
        final JTextField passwordField = pField;

        JButton createButton = new JButton("Create");
        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Credential newCred = new Credential(currentUser.getUsername(), domainField.getText(), usernameField.getText(), passwordField.getText());
                dal.addNewCredentialToUser(currentUser.getUsername(), newCred);
                removeCredentialCreateDialog();
                addUserMenu();

                mainFrame.revalidate();
            }
        });

        JPanel optionsPanel = new JPanel();
        optionsPanel.setLayout(new GridLayout(4, 1));

        final JSpinner passwordSize = new JSpinner(new SpinnerNumberModel(16, 8, 64, 1));
        final Checkbox lowerCase = new Checkbox("Lower Case", true);
        final Checkbox upperCase = new Checkbox("Upper Case", true);
        final Checkbox numbers = new Checkbox("Numbers", true);

        optionsPanel.add(passwordSize);
        optionsPanel.add(lowerCase);
        optionsPanel.add(upperCase);
        optionsPanel.add(numbers);

        JButton generatePasswordButton = new JButton("Generate Password");
        generatePasswordButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Option> options = new ArrayList<Option>();
                if (lowerCase.getState()) {
                    options.add(Option.LOWER_CASE);
                }
                if (upperCase.getState()) {
                    options.add(Option.UPPER_CASE);
                }
                if (numbers.getState()) {
                    options.add(Option.NUMBER);
                }
                int size = (Integer)passwordSize.getValue();

                String password = PasswordGenerator.generatePassword(size, options);
                passwordField.setText(password);

                mainFrame.revalidate();
            }
        });

        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel("Website:", JLabel.CENTER));
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));

        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(domainField);
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));

        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel("Username:", JLabel.CENTER));
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));

        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(usernameField);
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));

        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel("Password:", JLabel.CENTER));
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));

        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(passwordField);
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));

        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(generatePasswordButton);
        credCreatePanel.add(optionsPanel);
        credCreatePanel.add(new JLabel(""));

        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(createButton);
        credCreatePanel.add(new JLabel(""));
        credCreatePanel.add(new JLabel(""));

        mainFrame.add(credCreatePanel, BorderLayout.CENTER);
    }

    private void addLogoutButton()
    {
        JPanel logoutPanel = new JPanel();
        JButton logoutButton = new JButton("Logout");
        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentUser = null;
                mainFrame.setVisible(false);
                mainFrame.dispose();
                addLoginDialog();
            }
        });
        logoutPanel.add(logoutButton);
        mainFrame.add(logoutPanel, BorderLayout.SOUTH);
    }

    private void addEditDialog(Credential credential)
    {
        final Credential oldCred = credential;
        editPanel = new JPanel();
        editPanel.setLayout(new GridLayout(8, 5));
        final JTextField domainField = new JTextField(20);
        final JTextField usernameField = new JTextField(20);
        final JTextField passwordField = new JTextField(20);

        domainField.setText(credential.getDomain());
        usernameField.setText(credential.getUsername());
        passwordField.setText(credential.getPassword());
        domainField.setToolTipText("Website");
        usernameField.setToolTipText("Username");
        passwordField.setToolTipText("Password");
//
        JButton editButton = new JButton("Save Changes");
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Credential newCred = new Credential(currentUser.getUsername(), domainField.getText(), usernameField.getText(), passwordField.getText());
                dal.editCredential(currentUser.getUsername(), oldCred, newCred);
                removeEditDialog();
                addUserMenu();

                mainFrame.revalidate();
            }
        });

        JPanel optionsPanel = new JPanel();
        optionsPanel.setLayout(new GridLayout(4, 1));

        final JSpinner passwordSize = new JSpinner(new SpinnerNumberModel(16, 8, 64, 1));
        final Checkbox lowerCase = new Checkbox("Lower Case", true);
        final Checkbox upperCase = new Checkbox("Upper Case", true);
        final Checkbox numbers = new Checkbox("Numbers", true);

        optionsPanel.add(passwordSize);
        optionsPanel.add(lowerCase);
        optionsPanel.add(upperCase);
        optionsPanel.add(numbers);

        JButton generatePasswordButton = new JButton("Generate Password");
        generatePasswordButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Option> options = new ArrayList<Option>();
                if (lowerCase.getState()) {
                    options.add(Option.LOWER_CASE);
                }
                if (upperCase.getState()) {
                    options.add(Option.UPPER_CASE);
                }
                if (numbers.getState()) {
                    options.add(Option.NUMBER);
                }
                int size = (Integer)passwordSize.getValue();

                String password = PasswordGenerator.generatePassword(size, options);
                passwordField.setText(password);

                mainFrame.revalidate();
            }
        });


        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel("Website:", JLabel.CENTER));
        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));

        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));
        editPanel.add(domainField);
        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));

        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel("Username:", JLabel.CENTER));
        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));

        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));
        editPanel.add(usernameField);
        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));

        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel("Password:", JLabel.CENTER));
        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));

        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));
        editPanel.add(passwordField);
        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));

        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));
        editPanel.add(generatePasswordButton);
        editPanel.add(optionsPanel);
        editPanel.add(new JLabel(""));

        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));
        editPanel.add(editButton);
        editPanel.add(new JLabel(""));
        editPanel.add(new JLabel(""));

        mainFrame.add(editPanel, BorderLayout.CENTER);
    }

    private void removeUserMenu()
    {
        mainFrame.remove(menuPanel);
    }

    private void removeCredentialCreateDialog()
    {
        mainFrame.remove(credCreatePanel);
    }

    private void removeEditDialog()
    {
        mainFrame.remove(editPanel);
    }

    //Listeners
    private class EditLabelActionListener extends MouseAdapter{

        Credential credential;

        public EditLabelActionListener(Credential credential)
        {
            this.credential = credential;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            ((JLabel)e.getSource()).setBackground(Color.gray);
            removeUserMenu();
            addEditDialog(credential);

            mainFrame.revalidate();
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            ((JLabel)e.getSource()).setBackground(Color.lightGray);
        }

        @Override
        public void mouseExited(MouseEvent e) {
            ((JLabel)e.getSource()).setBackground(null);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            ((JLabel)e.getSource()).setBackground(Color.lightGray);
        }
    }

    private class DeleteLabelMouseListener extends MouseAdapter{

        Credential credential;

        public DeleteLabelMouseListener(Credential credential)
        {
            this.credential = credential;
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            ((JLabel)e.getSource()).setBackground(Color.gray);
            int response = JOptionPane.showOptionDialog(null, "Are you sure you want to delete your credential for "+credential.getDomain()+"?",
                    "Confirm Delete", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

            if(response==0)
            {
                dal.deleteCredential(currentUser.getUsername(), credential);
                removeUserMenu();
                addUserMenu();

                mainFrame.revalidate();
            }
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            ((JLabel)e.getSource()).setBackground(Color.lightGray);
        }

        @Override
        public void mouseExited(MouseEvent e) {
            ((JLabel)e.getSource()).setBackground(null);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            ((JLabel)e.getSource()).setBackground(Color.lightGray);
        }
    }

    private class PasswordLabelMouseListener extends MouseAdapter{

        String password;
        public PasswordLabelMouseListener(String password)
        {
            this.password = password;
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            ((JLabel)e.getSource()).setBackground(Color.gray);
            StringSelection stringSelection = new StringSelection(password);
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(stringSelection, null);
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            ((JLabel)e.getSource()).setBackground(Color.lightGray);
        }

        @Override
        public void mouseExited(MouseEvent e) {
            ((JLabel)e.getSource()).setBackground(null);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            ((JLabel)e.getSource()).setBackground(Color.lightGray);
        }
    }
}
