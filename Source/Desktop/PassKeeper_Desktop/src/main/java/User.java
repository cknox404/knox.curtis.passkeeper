import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public class User implements Serializable {
    String username;
    String password;

    public User(String username, String password)
    {
        this.username = username;
        this.password = password;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }
}
