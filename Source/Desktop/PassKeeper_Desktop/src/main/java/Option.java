import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: cknox
 */
public enum Option {
    LOWER_CASE(26, 97), UPPER_CASE(26, 65), NUMBER(10, 48);

    private int size;
    private int index;
    private Random random;
    private Option(int size, int index)
    {
        this.size = size;
        this.index = index;
        this.random = new Random(System.currentTimeMillis());
    }

    public char getRandomChar()
    {
        return Character.toChars(random.nextInt(size)+index)[0];
    }
}
